import throttle from "./throttle.js"
import getLocalStorage from "./getLocalStorage.js"



const formSearch = document.querySelector('.section-search__form-header')

function showCards(arr,id){
        arr.forEach(element=>{
            if (element.id.includes(`${id}`)) {
                element.classList.remove('modal-card--dont-active')
            }
        })
    }

 function hideCards(arr,id){
            arr.forEach(element=>{
                if (element.id.includes(`${id}`)) {
                    element.classList.add('modal-card--dont-active')
                }
            })
        } 

function getValue(arrElement){
  return  arrElement.map(element=>{
        if(element.value.trim().toLowerCase() !== 'all' && element.value.trim().toLowerCase() !== ''){
                    return element.value.trim().toLowerCase()
                }
    }).filter(elem => elem !== undefined)
}


function enumerationData(arrValue,arrModalCars){
    getLocalStorage().forEach(element => {
        const {id, ...anouther} = element
        const stringWithData = Object.values(anouther).join(' ').toLowerCase()
        let validDataWithCard = element => stringWithData.includes(element)
        if (arrValue.every(validDataWithCard)) {
            showCards(arrModalCars,id)
        }else{
            hideCards(arrModalCars,id)
        }
    })
}

function filter(event) {
    const arrElementForm = [...event.target.closest('.form-header').elements]
    const arrValue = getValue(arrElementForm)
    const modarCards = document.querySelectorAll('.modal-card')
    enumerationData(arrValue,modarCards)   
}



filter = throttle(filter,300)
formSearch.addEventListener('input',filter)

export {filter} 


