import Form from "../elements/form/Form.js"
import Label from "../elements/form/label/Label.js"
import Button from "../elements/button/Button.js"
import Select from "../elements/form/select/Select.js"
import Input from "../elements/form/input/Input.js"



const formForCreateCard = () =>{
    const btn = document.querySelector('.section-nav-men__btn')
    const body = document.querySelector('body');
    const form = new Form({
        className:['main__form', 'form'],
        formData:{
          labels:[
            new Label({
              className: ['main__form-label', 'form-label-purpose-visit'],
              attribut: '#',
              text: 'Цель визита'
            }),
            new Label({
              className: ['main__form-label', 'form-label-name-description'],
              attribut: '#',
              text: 'Краткое описание визита'
            }),
            new Label({
              className: ['main__form-label', 'form-label-urgency'],
              attribut: '#',
              text: 'Cрочность'
            }),
            new Label({
              className: ['main__form-label', 'form-label-name'],
              attribut: '#',
              text: 'Имя'
            }),
            new Label({
              className: ['main__form-label', 'form-label-last-name'],
              attribut: '#',
              text: 'Фамилия'
            }),
            new Label({
              className: ['main__form-label', 'form-label-patronymic'],
              attribut: '#',
              text: 'Отчество'
                }),
            new Label({
              className: ['main__form-label', 'form-label-doctor'],
              attribut: '#',
              text: 'Выберите доктора'
                }),
          ],
          inputsAndSelect:[
            new Input({
              className: ['main__form-input', 'form-input-purpose-visit'],
              type: 'text',
              name: 'purposeVisit',
            }),
            new Input({
              className: ['main__form-input', 'form-input-description'],
              type: 'text',
              name: 'description',
            }),
            new Select({
              className: ['main__form-select', 'form-select-urgency'],
              name: 'urgency',
              content:`
              <option value="Обычная">Обычная</option>
              <option value="Приоритетная">Приоритетная</option>
              <option value="Неотложная">Неотложная</option>
              `,
            }),
             new Input({
               className: ['main__form-input', 'form-input-name'],
               type: 'text',
               name: 'patientName',
               placeholder: 'Иван'
             }),
             new Input({
               className: ['main__form-input', 'form-input-last-name'],
               type: 'text',
               name: 'patientLastName',
               placeholder: 'Иванов'
             }),
             new Input({
              className: ['main__form-input', 'form-input-patronymic'],
              type: 'text',
              name: 'patientPatronymic',
              placeholder: 'Иванович'
            }),
            new Select({
              className: ['main__form-select', 'form-select-patronymic'],
              name: 'doctor',
              content:`
              <option selected="selected" value="Кардиолог">Кардиолог</option>
              <option value="Стоматолог">Стоматолог</option>
              <option value="Терапевт">Терапевт</option>
              `,
              additionalDataForForm:{
                therapist:{
                  labels:[
                    new Label({
                      className: ['main__form-label', 'form-label-age'],
                      attribut: '#',
                      text: 'Полных лет'
                     }),
                    ],
                    inputs:[
                      new Input({
                      className: ['main__form-input', 'form-input-age'],
                      type: 'number',
                      name: 'patientAge',
                        }),
                    ]
                },
                cardiologist:{
                  labels:[
                    new Label({
                      className: ['main__form-label', 'form-label-pressure'],
                      attribut: '#',
                      text: 'Обычное давление'
                     }),
                     new Label({
                      className: ['main__form-label', 'form-label-weight'],
                      attribut: '#',
                      text: 'Вес'
                     }),
                     new Label({
                      className: ['main__form-label', 'form-label-growth'],
                      attribut: '#',
                      text: 'Рост'
                     }),
                     new Label({
                      className: ['main__form-label', 'form-label-age'],
                      attribut: '#',
                      text: 'Полных лет'
                     }),
                     new Label({
                      className: ['main__form-label', 'form-label-last-diseases'],
                      attribut: '#',
                      text: 'Перенесенные заболевания сердечно-сосудистой системы'
                     }),
                    ],
                    inputs:[
                      new Input({
                      className: ['main__form-input', 'form-input-pressure'],
                      type: 'number',
                      name: 'patientPressure',
                        }),
                      new Input({
                      className: ['main__form-input', 'form-input-weight'],
                      type: 'number',
                      name: 'patientMass',
                        }),
                      new Input({
                      className: ['main__form-input', 'form-input-growth'],
                      type: 'number',
                      name: 'patientGrowth',
                        }),
                      new Input({
                        className: ['main__form-input', 'form-input-age'],
                        type: 'number',
                        name: 'patientAge',
                          }),
                      new Input({
                      className: ['main__form-input', 'form-input-last-diseases'],
                      type: 'text',
                      name: 'lastDiseases',
                        }),    
                    ]
                },
                dentist:{
                  labels:[
                    new Label({
                      className: ['main__form-label', 'form-label-date-last-visit'],
                      attribut: '#',
                      text: 'Дата последнего посещения'
                     }),
                    ],
                    inputs:[
                      new Input({
                      className: ['main__form-input', 'form-input-last-visit'],
                      type: 'date',
                      name: 'patientLastVisit',
                        }),
                    ]
                },
                }
            })
             ],
             btnSubmit:[
              new Button({
                className: ['main__form-btn','btn', 'btn-primary'],
                type: 'submit',
                text: 'Записать',
              }),
              new Button({
                className: ['main__form-btn-close', 'btn-closes'],
                type: 'button',
              })
              ],
        }
      })
    
      function createModalForCreateCards(){
          form.render('.main', 'afterbegin')
          setTimeout(() =>{
           document.querySelector(`.${form.element.classList[0]}`).classList.remove('form')
          },0)
          body.classList.remove('stop-scroll');
          document.querySelector('.main__block-for-form').classList.add("block-for-form")
      }
      
      btn.addEventListener('click', createModalForCreateCards)
    }

export default formForCreateCard