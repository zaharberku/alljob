import CreateHTMLElement from '../../CreateHTMLElement.js'
import Valid from '../../../function/class/Valid.js'
import Lisener from '../../../function/class/Lisener.js';

export default class Input extends CreateHTMLElement {
    constructor({ className = '', id = '', type = '', placeholder = '', name = '', value = ''}) {
      super(className, id);
      this.type = type;
      this.placeholder = placeholder;
      this.name = name;
      this.value = value;
    };
    render(elem, place) {
      this.createInput();
      this.addElement(elem, place);
      this.lisenerInput()
    };
    createInput() {
      this.createElement({
        name: 'input',
        attributes: {
          value: this.value,
          name: this.name,
          type: this.type,
          placeholder: this.placeholder,
        },
      });
    };
    isValidData(element){
      const func = new Valid(element.value, element)
      func.render(element.name)
    }
    lisenerInput(){
      Lisener.render(`.${this.className[1]}`, 'input' ,this.isValidData.bind(this))
    }
  }