import CreateHTMLElement from '../CreateHTMLElement.js'
import VisitCardiologist from '../cards/VisitCardiologist.js'
import VisitDentist from '../cards/VisitDentist.js'
import VisitTherapist from '../cards/VisitTherapist.js'
import Fetch from '../../api/Fetch.js'
import Valid from '../../function/class/Valid.js'
import addLocalStorage from '../../function/addLocalStorage.js'
import renderCard from '../../function/renderCard.js'
import Lisener from '../../function/class/Lisener.js'



export default class Form extends CreateHTMLElement {
    constructor({ className, id , formData:{ inputsAndSelect, labels, btnSubmit} }) {
      super(className, id);
      this.inputsAndSelect = inputsAndSelect;
      this.btnSubmit = btnSubmit;
      this.labels = labels
    }
    render(elem, place) {
      this.createForm();
      this.addElement(elem, place);
      this.addFields();
      this.listenerSubmit();
    }
    createForm() {
      this.createElement({
        name: 'form',
        attributes: {
          name: 'registration-form'
        }
      })
    }
    addFields() {
      this.labels.forEach(element => element.render(`.${this.className[0]}`, 'beforeend'));
      this.btnSubmit.forEach(element => {
        if (element.className[0] === 'main__form-btn-close') {
          element.render(`.${this.className[0]}`, 'afterbegin',`.${this.className[0]}`)
        }else{
          element.render(`.${this.className[0]}`, 'beforeend')
        }
      });
      this.inputsAndSelect.forEach((element,index) => element.render(`.${this.labels[index].className[1]}`,'beforeend'));
    }
  
    isValidValue(event, arr){
      arr.forEach((elem)=>{
      const func = new Valid(event.target.elements[elem].value, event.target.elements[elem])
      func.render(elem);
      })
    }

    getValue(event, arr){
      const data = {}
      let mass = null
      let growth = null
      arr.forEach(element => {
        if(element === "patientMass"){
          mass = event.target.elements[element].value
        }else if (element === "patientGrowth") {
          growth = event.target.elements[element].value
        }else{
          data[element] = event.target.elements[element].value
        }
        if(mass && growth) {
          data["bodyMassIndex"] = (mass / ((growth * 0.01)**2)).toFixed(2)
        }
     });
     return data
    }

    static renderModalCard(data){
      const {id, ...another} = data
      const readyData = {id:`card-${id}`, className: ["section-modal-creat-cards__modal-card", "modal-card", "card"], ...another}
      let renderCardDoctor = null;
      if(data.doctor === 'Кардиолог'){
        renderCardDoctor = new VisitCardiologist(readyData)
      }else if(data.doctor === 'Терапевт'){
        renderCardDoctor = new VisitTherapist(readyData)
      }else if(data.doctor === 'Стоматолог'){
        renderCardDoctor = new VisitDentist(readyData)
      }
      renderCardDoctor.render('.main__section-modal-creat-cards', 'beforeend')
    }

    postAndCreateData(event,arrName){
      Fetch.createData(this.getValue(event,arrName))
          .then(resolve => resolve.json())
          .then(data => {
            Form.renderModalCard(data)
            addLocalStorage(data)
          })
          .then(() => {
            const body = document.querySelector('body');
            const form = document.querySelector('.main__form')
            const titleText = document.querySelector('.main__title-text')
            form.remove();
            body.classList.add('stop-scroll');
            body.querySelector('.main__block-for-form').classList.remove("block-for-form")
            if (titleText) {
              titleText.remove()
            }
          })
    }

    getAndValidDataWithForm(element,event){
      event.preventDefault();
      const arrName = [...element.elements].filter(elem => elem.name).map(elem => elem.name)
      this.isValidValue(event, arrName)
      if(!element.querySelector('.main__not-valid-input')){
        this.postAndCreateData(event,arrName)
      }
    }


    errorOutput(form){
      form.lastChild.insertAdjacentHTML('beforebegin','<p>Некорректная почта или пароль, попробойте ещё раз</p>')     
    }

    async validPassword(obj,form){
     await Fetch.getToken(obj)
     .then(resolve=>{
       if(resolve.ok){
        form.remove()
        document.querySelector('.section-nav-men__btn').innerHTML = 'Создать карточку'
        document.querySelector('.main__block-for-form').classList.remove("block-for-form")
        return resolve.text()
       }else{
        this.errorOutput(form)
       }
     })
     .then(token => {
       localStorage.setItem('token',Fetch.assignToken(token))
       renderCard()
      })
     
    }

    getDataWithInput(element,event){
      event.preventDefault()
     const objValue = [...element.elements].reduce((item,elem)=>{
      if(elem.name){
        item[elem.name] = elem.value
      } 
      return item
    },{})

    this.validPassword(objValue,element)
    }

    listenerSubmit(){
      if(this.className[0] === 'main__form'){
        Lisener.render(`.${this.className[0]}`,'submit',this.getAndValidDataWithForm.bind(this))
      }else if(this.className[0] === 'main__form-authorization'){
        Lisener.render(`.${this.className[0]}`,'submit',this.getDataWithInput.bind(this))
      }
    }
  
  }