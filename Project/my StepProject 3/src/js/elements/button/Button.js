import CreateHTMLElement from '../CreateHTMLElement.js'
import Lisener from '../../function/class/Lisener.js';
import Fetch from '../../api/Fetch.js';
import getLocalStorage from '../../function/getLocalStorage.js';
import Valid from '../../function/class/Valid.js';

export default  class Button extends CreateHTMLElement {
  constructor({ className, id, type = '', text = '' }) {
    super(className, id);
    this.type = type;
    this.text = text;
  }
  render(elem, place, nameRemoveBlock = null) {
    this.createButton()
    this.addElement(elem, place)
    if (nameRemoveBlock) this.listenerBtnClose(nameRemoveBlock)
    if (this.className[0] === "main__from-btn-more") this.listenerBtnMore(elem)
    if (this.className[0] === "main__from-btn-edit") this.lisenerBtnEdit(elem)
  }
  createButton() {
    this.createElement({
      name: 'button',
      attributes: {
        type: this.type,
        innerHTML: this.text,
      }
    })
  }
  stopScroll() {
    const body = document.querySelector('body');
    body.classList.add('stop-scroll');
    body.querySelector('.main__block-for-form').classList.remove("block-for-form")
  }

  removeWithLocalStorage(id){ 
  const newDataPatient = getLocalStorage().filter(element=> element.id !== id )
  localStorage.setItem('patient',JSON.stringify(newDataPatient))
  if(newDataPatient.length === 0){
    document.querySelector('.main').insertAdjacentHTML('beforeend','<h1 class="main__title-text">O items have been added</h1>') 
  }
  }

  closeForm(name, linkBtn) {
    const element = linkBtn.closest(name)
    if (name === '.main__form' || name === '.main__form-authorization') {
      this.stopScroll()
    }else if(element.id){
      const id = parseInt(element.id.split('-').reverse().join(' '))
      Fetch.deleteCard(id)
      .then(()=>{
        this.removeWithLocalStorage(id)
        element.remove()
      })
      
    }
    element.remove()
  }

  addMoreInfo(element, event) {
    if (element.classList.contains("main__from-btn-more")) {
      const blockWithMoreInfo = event.currentTarget.querySelector('div:last-of-type')
      blockWithMoreInfo.classList.toggle('div-with-add-info')
      if (blockWithMoreInfo.classList.contains('div-with-add-info')) {
        element.innerHTML = 'More'
        event.currentTarget.classList.add('section-modal-creat-cards__modal-card')
      }else{
        element.innerHTML = 'Hide'
        event.currentTarget.classList.remove('section-modal-creat-cards__modal-card')
      }
    }
  }

  listenerBtnMore(name) {
    Lisener.render(name, 'click', this.addMoreInfo)
  }

  isValidNewData(value,name,element){
    const func = new Valid(value, element)
    func.render(name);
  }
  editWithLocalStorage(data){
    const newDataPatientLocalStorage = getLocalStorage().map(element => element.id === data.id ? element = data : element)
    localStorage.setItem('patient',JSON.stringify(newDataPatientLocalStorage))
  }

  async saveEditCard(newData,arr, id){
   const data = await Fetch.putCard(newData,id)
    arr.forEach(this.removeAttribute);
    this.editWithLocalStorage(data)
  }
  removeAttribute(element){
    element.removeAttribute('contenteditable')
    element.classList.remove('text')
  }

  setAttribute(element){
    element.setAttribute('contenteditable', 'true')
  }

  editTextCard(element,event) {
    if (element.classList.contains("main__from-btn-edit")){
      const blockWithMoreInfo = event.currentTarget.querySelector('div:last-of-type')
      const btnMore = event.currentTarget.querySelector(".main__from-btn-more")
      const parent = event.currentTarget
      const id = parseInt(parent.id.split('-').reverse().join(' '))
      const arrElementP = parent.querySelectorAll('p')
      let result = null;


      btnMore.disabled = true
      event.currentTarget.classList.remove('section-modal-creat-cards__modal-card')
      blockWithMoreInfo.classList.remove('div-with-add-info')
      element.innerHTML = 'Save'
      
      const newObjWithData = [...arrElementP].reduce((item,elem)=>{
        let name = elem.dataset.patient
        let value = elem.innerHTML
        if (!elem.getAttribute('contenteditable')) {
          this.setAttribute(elem)
          elem.classList.add('text')
        }else{
          this.isValidNewData(value,name,elem)
          result = true
        }
        item[name] = value
        return item
      },{})

      if (!parent.querySelector('.main__not-valid-input') && result) {
          btnMore.disabled = false
          blockWithMoreInfo.classList.add('div-with-add-info')
          event.currentTarget.classList.add('section-modal-creat-cards__modal-card')
          element.innerHTML = 'Edit'
          this.saveEditCard(newObjWithData, arrElementP, id)
        }
      
    }
  }

  lisenerBtnEdit(name) {
    Lisener.render(name, 'click', this.editTextCard.bind(this))
  }

  listenerBtnClose(name) {
    const div = document.querySelector(name)
    div.addEventListener('click', (event) => {
      if (event.target.classList.contains("btn-closes")) {
        this.closeForm(name, event.target)
      }
    })
  }
}
