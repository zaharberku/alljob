import CreateHTMLElement from '../CreateHTMLElement.js'
import Button from '../button/Button.js'

export default class Visit extends CreateHTMLElement {
    constructor({ className, id, purposeVisit, description, urgency, patientName, patientLastName, patientPatronymic, doctor }) {
      super(className, id);
      this.purposeVisit = purposeVisit;
      this.description = description;
      this.urgency = urgency;
      this.name = patientName;
      this.lastName = patientLastName;
      this.patronymic = patientPatronymic;
      this.doctor = doctor;
    }
    render(elem, place) {
      this.createVisit()
      this.addElement(elem, place)
      this.createCloseBtn()
      this.createBtnMore()
      this.createBtnEdit()
    }
    createVisit(string){
      this.createElement({
        name: 'div',
        attributes: {
          innerHTML: `
          <div class='modal-card__info'>
          <ul class="modal-card__item">
              <li class="modal-card__list list"><span class="list__title">Доктор:</span><p class="list__text" data-patient="doctor">${this.doctor}</p></li>
              <li class="modal-card__list list"><span class="list__title">Имя:</span><p class="list__text" data-patient="patientName">${this.name}</p></li>
              <li class="modal-card__list list"><span class="list__title">Фамилия:</span><p class="list__text" data-patient="patientLastName">${this.lastName}</p></li>
              <li class="modal-card__list list"><span class="list__title">Отчество:</span><p class="list__text" data-patient="patientPatronymic">${this.patronymic}</p></li>
              <li class="modal-card__list list"><span class="list__title">Срочность:</span><p class="list__text" data-patient="urgency">${this.urgency}</p></li>
          </ul>
      </div>
        ${string}
      `
        }
      });
    }
    createCloseBtn() {
      new Button({
        className: ['modal-card__btn-close','btn-closes'],
        type: 'button',
      }).render(`#${this.id}`, 'afterbegin', `#${this.id}`)
    }
    createBtnMore() {
      new Button({
        className: ['main__from-btn-more','modal-card-btn', 'btn', 'btn-primary'],
        type: 'button',
        text: 'More'
      }).render(`#${this.id}`, 'beforeend')
    }
    createBtnEdit() {
      new Button({
        className: ['main__from-btn-edit','modal-card-btn', 'btn', 'btn-primary'],
        type: 'button',
        text: 'Edit'
      }).render(`#${this.id}`, 'beforeend')
    }
  }