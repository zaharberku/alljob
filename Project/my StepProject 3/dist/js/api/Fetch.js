export default class Fetch {
  constructor(){
    this.token = null
  }
  static assignToken(token){
    this.token = token
    return this.token
  }
  static createData(dataPatient) {
    
    document.querySelector('.main__form-btn').disabled = true;
    return fetch("https://ajax.test-danit.com/api/v2/cards", {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.token}`
      },
      body: JSON.stringify(dataPatient)
    })
  }
  static getDataCards() {
    return fetch("https://ajax.test-danit.com/api/v2/cards", {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.token}`
      }
    })
      .then(resolve => resolve.json())
  }
  static getDataCard(card) {
    return fetch(`https://ajax.test-danit.com/api/v2/cards/${card}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.token}`
      }
    })
  }
  static deleteCard(id) {
    
   return fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
      method: 'DELETE',
      headers: {
        'Authorization': `Bearer ${this.token}`
      },
    })
  }
  static putCard(newData,id) {
    return fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${this.token}`
      },
      body: JSON.stringify(newData)
    })
    .then(resolve => resolve.json())
   }
   static getToken({userEmail,userPassword}){
    return fetch("https://ajax.test-danit.com/api/v2/cards/login", {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ email: `${userEmail}`, password: `${userPassword}`})
    })
   }
}



// c0037d39-ae76-45c2-9c8b-6e489ae4cd05