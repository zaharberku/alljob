export default class CreateHTMLElement {
    constructor(className, id) {
      this.className = className;
      this.id = id;
      this.element = null;
    };
    render(elem, place) {
      this.createElement();
      this.addElement(elem, place);
    };
  
    createElement({ name, attributes }) {
      this.element = document.createElement(name);
      if(this.id){
      this.element.id = this.id 
      }
      if(this.className){
        this.element.className = this.className.join(' ');
      }
      Object.keys(attributes).forEach(key => {
        this.element[key] = attributes[key]
      });
    };
    addElement(container, place) {
      document.querySelector(container).insertAdjacentHTML(place, this.element.outerHTML);
    };
  };


