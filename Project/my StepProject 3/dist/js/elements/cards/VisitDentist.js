import Visit from "./Visit.js";

export default class VisitDentist extends Visit {
    constructor({ className, id, purposeVisit, description, urgency, patientName, patientLastName, patientPatronymic, patientLastVisit,doctor }) {
      super({ className, id, purposeVisit, description, urgency, patientName, patientLastName, patientPatronymic,doctor })
      this.lastVisit = patientLastVisit.split('-').reverse().join('-');
    }
    render(elem, place) {
      this.createVisitDentist();
      this.addElement(elem, place)
      this.createCloseBtn()
      this.createBtnMore()
      this.createBtnEdit()
    }
    createVisitDentist() {
      this.createVisit(`
      <div class='modal-card__info div-with-add-info'>
      <ul class="modal-card__list">
          <li class="modal-card__list list"><span class="list__title">Цель:</span><p class="list__text" data-patient="purposeVisit">${this.purposeVisit}</p></li>
          <li class="modal-card__list list"><span class="list__title">Описание:</span><p class="list__text" data-patient="description">${this.description}</p></li>
          <li class="modal-card__list list"><span class="list__title">Дата последнего визита:</span><p class="list__text" data-patient="patientLastVisit">${this.lastVisit}</p></li>
      </ul>
    </div>
  `)
    }
  }