import Visit from "./Visit.js";

export default class VisitTherapist extends Visit {
    constructor({ className, id, purposeVisit, description, urgency, patientName, patientLastName, patientPatronymic, patientAge,doctor }) {
      super({ className, id, purposeVisit, description, urgency, patientName, patientLastName, patientPatronymic,doctor })
      this.age = patientAge;
    }
    render(elem, place) {
      this.createVisitTherapist();
      this.addElement(elem, place)
      this.createCloseBtn()
      this.createBtnMore()
      this.createBtnEdit()
    }
    createVisitTherapist() {
      this.createVisit(`
  <div class='modal-card__info div-with-add-info'>
      <ul class="modal-card__list">
          <li class="modal-card__list list"><span class="list__title">Цель:</span><p class="list__text" data-patient="purposeVisit">${this.purposeVisit}</p></li>
          <li class="modal-card__list list"><span class="list__title">Описание:</span><p class="list__text" data-patient="description">${this.description}</p></li>
          <li class="modal-card__list list"><span class="list__title">Возраст:</span><p class="list__text" data-patient="patientAge">${this.age}</p></li>
      </ul>
  </div>
  `)
    }
  }