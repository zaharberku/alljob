import getLocalStorage from "./getLocalStorage.js"

function addLocalStorage(data){
    const all = getLocalStorage()
    all.push(data)
    localStorage.setItem('patient',JSON.stringify(all))
}

export default addLocalStorage
