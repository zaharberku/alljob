import Fetch from '../api/Fetch.js'
import Form from '../elements/form/Form.js'
import addLocalStorage from './addLocalStorage.js'

async function renderCard() {
    const data = await Fetch.getDataCards().then(resolve => resolve);
    const titleText = document.querySelector('.main__title-text')
    if (data.length !== 0) {
        if (titleText) {
            titleText.remove()
        }
        localStorage.removeItem('patient')
        data.forEach(element => {
            addLocalStorage(element)
            Form.renderModalCard(element)
        });
    } else {
        document.querySelector('.main').insertAdjacentHTML('beforeend','<h1 class="main__title-text">O items have been added</h1>') 
    }
}

export default renderCard