import Form from "../../elements/form/Form.js"
import Label from "../../elements/form/label/Label.js"
import Input from "../../elements/form/input/Input.js"
import Button from "../../elements/button/Button.js"

const createModalAuthorization = () => {
  const btn = document.querySelector('.section-nav-men__btn')
  const body = document.querySelector('body');


const form = new Form({
    className:['main__form-authorization', 'form', 'form--authorization'],
    formData:{
      labels:[
        new Label({
          className: ['main__form-label', 'form-label-email'],
          attribut: '#',
          text: 'Email'
        }),
        new Label({
          className: ['main__form-label', 'form-label-password'],
          attribut: '#',
          text: 'Пароль'
        }),
      ],
      inputsAndSelect:[
        new Input({
          className: ['main__form-input', 'form-input-email'],
          type: 'email',
          name: 'userEmail',
        }),
        new Input({
          className: ['main__form-input', 'form-input-password'],
          type: 'password',
          name: 'userPassword',
        }),
         ],
         btnSubmit:[
          new Button({
            className: ['main__form-modal-authorization-btn','btn', 'btn-primary'],
            type: 'submit',
            text: 'Войти',
          }),
          new Button({
            className: ['main__form-btn-close', 'btn-closes'],
            type: 'button',
          })
          ],
    }
  })


function createModal(){
  document.querySelector('.main__block-for-form').classList.add("block-for-form")
    form.render('.main', 'afterbegin')
}
body.classList.remove('stop-scroll');
btn.addEventListener('click',createModal)
}


export default createModalAuthorization