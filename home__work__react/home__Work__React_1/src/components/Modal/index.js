import React,{Component} from "react";


class Modal extends Component{
    stopBubbling = (event)=>{
        event.stopPropagation()
    }
    render(){
        const {dataForModal:{actions,closeButton,text,title}} = this.props
        return(
            <div className="main__modal modal" onClick={(event)=>this.stopBubbling(event)}>
                <div className="modal__header">
                    <h1 className="modal__title">{title}</h1>
                    {closeButton}
                </div>
                <div className="modal__body">
                    {text}
                </div>
                <div className="modal__footer">
                    {actions}
                </div>
            </div>
        )
    }
}

export default Modal