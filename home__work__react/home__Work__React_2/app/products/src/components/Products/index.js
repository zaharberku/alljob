import React,{Component} from "react";
import PictureSelection from '../themes'
// import {addElementLocalStorage,getElementLocalStorage} from '../localStorage'

class Products extends Component{
    state = {
            type:'star',
            fill:'#fff',
            stroke:'black',
    }

    favoritesProduct = (event, dataProtuct) =>{
        const {deletElementWithLocalStorage} = this.props
        const {fill} = this.state
        if(fill === '#000') {
            deletElementWithLocalStorage(event,dataProtuct.id,'favorite')
        }
        this.setState({
                fill: this.state.fill === '#000' ? '#fff' : '#000',
        })
    }

    render(){
        const {dataProtuct, button, openModal} = this.props
        const {type,fill,stroke} = this.state
        return(
            <div className="main__basket products" onClick={openModal}>
                <div className="products__header">
                    <PictureSelection fill={fill} type={type} stroke={stroke} favoritesProduct={(event)=>this.favoritesProduct(event,dataProtuct)}/>
                </div>
                <div className="products__body">
                    <p>{dataProtuct.name}</p>
                    <img className="products__img" src={dataProtuct.pathPicture} alt={dataProtuct.name}/>
                    <p>Цвет: {dataProtuct.color}</p>
                    <p>Артикл: {dataProtuct.vendorCode}</p>
                    <p>Цена: {dataProtuct.price}</p>
                </div>
                <div className="products__footer">
                    {button}
                </div>
            </div>
        )
    }
}


export default Products