import React,{Component} from "react";
import ProductsInBasket from '../ProductsInBasket'


class Favorite extends Component{
    render(){
        const {data} = this.props
        return(
                <div className="main__favorite favorite">
                    <h1 className="favorite__title">Favorite</h1>
                    <div>
                        <ul className="favorite__list">
                            {data.map(({name,pathPicture,id})=> <ProductsInBasket key={name} name={name} img={pathPicture} favorite={false}/>)}
                        </ul>
                    </div>
                </div>
        
        )
    }
}

export default Favorite