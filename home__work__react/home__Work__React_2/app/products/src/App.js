import './App.scss';
import { Favorite ,Button, Modal, Products, Loading, Basket, getElementLocalStorage, addElementLocalStorage} from './components'
import React,{Component} from "react";



class App extends Component{
  state = {
    flag:false,
    text: null,
    dataProducts:[],
    dataForBasket:[],
    dataForFavorite:[],
    loading:true,
    dataForBtn:{
      background:"#e8b692",
      textBtnSecond:"Добавить продукт в корзину",
    },
    dataForModal:null
  }

  async componentDidMount(){
      const data = await fetch("./products.json").then(response=> response.json())
      this.setState({
        dataProducts:data,
        loading:false,
        dataForBasket:getElementLocalStorage('basket'),
        dataForFavorite:getElementLocalStorage('favorite')
      })
  }

  modalWindow = () =>{
    const modalWindowDeclarations = [
      {
        id:1,
        text:'Продукт добален в избраное',
        title:'Title1',
        actions:<button className="modal__footer-btn footer-btn">More</button>,
        closeButton:<button onClick={this.closeModal.bind(this)} className="modal__header-btn header-btn">X</button>
      },
      {
        id:2,
        text:'Продукт добален в корзину',
        title:'Title2',
        actions:<button className="modal__footer-btn footer-btn">More</button>,
        closeButton:<button onClick={this.closeModal.bind(this)} className="modal__header-btn header-btn">X</button>
      },
    ]
    return modalWindowDeclarations
  }

  closeModal(){
    document.querySelector('html').classList.remove('stop-scroll')
    this.setState({
      flag:false,
      dataForModal:null
    })
  }
  filter(id, data){
    const {dataProducts} = this.state
    const newElement = dataProducts.filter(element=> element.id === id)
    const oldElement = data
    return oldElement.some(element => element === newElement[0])
  }

  filterProduct(id, data, keys){
    if (!this.filter(id, data)) {
    const {dataProducts} = this.state
    const newElement = dataProducts.filter(element=> element.id === id)
      addElementLocalStorage(keys,newElement[0])
      return data.concat(newElement) 
    }
    return data
  }

  addOnPageDataFavorite = (id, data) =>{
    this.setState({dataForFavorite: this.filterProduct(id, data, 'favorite')})
  }

  addOnPageDataBasket = (id, data) =>{
    this.setState({dataForBasket: this.filterProduct(id, data, 'basket')})
  }

  openModal = (event,id) =>{
    const {dataForFavorite,dataForBasket} = this.state
    const {id:idBasket} = event.target.dataset
    const {id:idFavorite} = event.target.closest('span')?.dataset ?? {id:null}
    const dataId = (idBasket || idFavorite)
    const data = +dataId === 1 ? dataForFavorite : dataForBasket
    const bool = dataForFavorite.some(element => element.id === id) 
    if (dataId && !bool) {
    document.querySelector('html').classList.add('stop-scroll')
    this.setState({
      flag:true,
      dataForModal: this.modalWindow().find(element => element.id === +dataId),
    })
    if (+dataId === 1) {
      this.addOnPageDataFavorite(id, data)
    }else if (+dataId === 2) {
      this.addOnPageDataBasket(id, data)
    }
  }
  }

  renderProduct = () =>{
    const {dataForBtn, dataProducts} = this.state
    return(
      dataProducts.map(element=> <Products key={element.id} deletElementWithLocalStorage={this.deletElementWithLocalStorage} openModal={(event)=>this.openModal(event,element.id)} dataProtuct={element} button={<Button dataForBtn={dataForBtn}/>}/>)
    )
  }

  deletElementWithLocalStorage = (event,id,keys) => {
    if (event.target.tagName === 'BUTTON' || event.target.closest('span')) {
       const newLocalStorage =  getElementLocalStorage(keys).filter(element=>element.id !== id)
       localStorage.setItem(keys, JSON.stringify(newLocalStorage))
       if(keys === 'basket'){
        this.setState({dataForBasket:newLocalStorage})
       }else if(keys === 'favorite'){
        this.setState({dataForFavorite:newLocalStorage})
       }
    }
  }

  render(){
    const {dataForModal, flag, text, loading, dataForBasket,dataForFavorite} = this.state
    return (
      <div className="App">
        <main className='App__main main'>
          <Favorite data={dataForFavorite} />
        {flag && <div onClick={this.closeModal.bind(this)} className='main__window-close'><Modal dataForModal={dataForModal} text={text}/></div>}
          <div className='main__block-with-products'>
        {loading ? <Loading/> : this.renderProduct()}
        </div>
        <Basket data={dataForBasket} deletElementWithLocalStorage={this.deletElementWithLocalStorage} />
        </main>
      </div>
    );
  }
 
}

export default App;
