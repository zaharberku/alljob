import React,{Component} from "react";


class Button extends Component{
    render(){
        const{data:{text,background,id},openModal}= this.props;

        return(
            <button className="header__btn" data-id={id} onClick={(event)=>openModal(event)} background={background}>{text}</button>

           )
    }
}

export default Button