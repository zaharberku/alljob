import React,{Component} from "react";
import Img from "../Img";
import style from "./Product.module.scss"
import Button from "../Button";

class Product extends Component{
    button = () =>{
         const dataButton =  {
            id:1,
            background:"#e8b692",
            text:"Add to cart",
          }
        return dataButton
      }
    render(){
        const {data:{name, price, pathPicture, vendorCode, color},openModal} = this.props
        return(
            <div className={style.blockProduct}>
            <Img src={pathPicture} alt={name}/>
            <p><span className={style.list}>Название:</span>{name}</p>
            <p><span className={style.list}>Цена:</span>{price}</p>
            <p><span className={style.list}>Код товара:</span>{vendorCode}</p>
            <p><span className={style.list}>Цвет:</span>{color}</p>
            <Button data={this.button()} openModal={openModal}/>
            </div>
        )
    }
}
export default Product