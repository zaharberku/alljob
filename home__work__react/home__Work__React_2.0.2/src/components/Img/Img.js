import React,{Component} from "react";
import style from "./Img.module.scss"


class Img extends Component{
    render(){
        const {src, alt} = this.props 
        return(
          <img className={style.img} src={src} alt={alt}/>
        )
    }
}


export default Img