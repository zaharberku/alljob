import './App.scss';
import {Button, Modal, Product } from './components'
import React,{Component} from "react";



class App extends Component{
  state = {
    flag:false,
    dataForModal:[],
    dataForProducts:[]
  }

  componentDidMount = async() =>{
    const data = await fetch('./package.json').then(res=>res.json())
    this.setState({dataForProducts:data})
  }

  modalWindow = () =>{
    const modalWindowDeclarations = [
      {
        id:1,
        text:'Modal First',
        title:'Title',
        actions:<button className="modal__footer-btn">Save</button>,
        closeButton:<button onClick={this.closeModal.bind(this)} className="modal__header-btn">X</button>,
      },
      {
        id:2,
        text:'Second First',
        title:'Title2',
        actions:<button className="modal__footer-btn footer-btn">More</button>,
        closeButton:<button onClick={this.closeModal.bind(this)} className="modal__header-btn header-btn">X</button>
      },
    ]
    return modalWindowDeclarations
  }

  closeModal(){
    this.setState({
      flag:false,
      dataForModal:null
    })
  }

  openModal = (event) =>{
    const {id} = event.target.dataset
    this.setState({
      flag:true,
      dataForModal:this.modalWindow().find(element => element.id === +id)
    })
  }
  render(){
    const {dataForModal, flag, dataForProducts} = this.state
    return (
      <div className="App">
        <header className="App__header header">
        </header>
        <main className='App__main main'>
          {dataForProducts.map(element=><Product key={element.id} data={element} openModal={this.openModal}/>)}
          {flag && <div onClick={this.closeModal.bind(this)} className='main__window-close'><Modal dataForModal={dataForModal}/></div>}
        </main>
      </div>
    );
  }
 
}

export default App;
