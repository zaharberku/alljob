import React,{useState, useEffect} from 'react';
import './App.scss';
import { Header, Inbox } from './components';
import axios from 'axios';

const App = () =>{
	const [user, setUser] = useState({
		name:'Name',
		id:1,
	});
	const [email, setEmails] = useState([]);

	const deleteEmail = (id) => {
		setEmails(email.filter((e) => e.id !== id));
	};

	useEffect(()=>{
		axios.get("/email").then((response)=>{
			setEmails(response.data);
		})
	}, [])

	return (
		<div className="App">
			<Header user={user} />
			<Inbox emails={email} deleteEmail={deleteEmail} />
		</div>
	);

}

export default App;
