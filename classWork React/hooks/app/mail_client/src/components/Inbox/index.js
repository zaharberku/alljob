import React from 'react';
import Email from '../Email';

class Inbox extends React.Component {
	render() {
		const { emails, deleteEmail } = this.props;
		return (
			<div className="inbox">
				{emails.map((e) => (
					<Email key={e.id} email={e} deleteEmail={deleteEmail} />
				))}
			</div>
		);
	}
}

export default Inbox;
