import React from 'react';
import PropTypes from 'prop-types';
import Star from '../themes/star.js'

class Email extends React.Component {
	render() {
		const { email, deleteEmail } = this.props;
		return (
			<div className="email">
				<Star fill={'red'} stroke={'green'}/>
				<div>{email.topic}</div>
				<button onClick={() => deleteEmail(email.id)}>DELETE</button>
			</div>
		);
	}
}

Email.propTypes = {
	deleteEmail: PropTypes.func.isRequired,
	email: PropTypes.shape({
		topic: PropTypes.string,
		id: PropTypes.number,
	}),
};

export default Email;
