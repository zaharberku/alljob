import React,{ Component } from 'react';
import './App.scss';
import {Numbers} from './components'

class App extends Component{
  state = {
    numbers:[],
  }
  generatedNum = (min,max) =>{
    return Math.floor(min + Math.random() * (max+1-min))
  }
  generate = (min,max) =>{
    const rand = this.generatedNum(min,max)
    if (!this.state.numbers.includes(rand)) {
      this.setState({
        numbers: this.state.numbers.concat([rand])
      })
    } else if(this.state.numbers.length !== 6){
      this.generate(1,6)
    }
  }
  deleteNumber = (id) => {
    this.setState({
      numbers: this.state.numbers.filter(element => element !== +id)
    })
  }
  render(){
    const {numbers} = this.state
    return (
      <div className="App">
        <div className='App__block-with-number'>
          {numbers.map(element=>{
         return <Numbers key={element} number={element} id={element} deleteNumber={this.deleteNumber}/>
        })}</div>
        <button onClick={this.generate.bind(this,1,6)}>Generate</button>
      </div>
    );
  }
 
}

export default App;
