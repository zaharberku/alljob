import {Component} from 'react'


class Numbers extends Component{
    render(){
        const {number,id,deleteNumber} = this.props
        return(
            <div>
                <button onClick={deleteNumber.bind(null,id)}>x</button>
                {number}
            </div>
        )
    }
}

export default Numbers