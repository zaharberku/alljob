import React, {Component} from "react";
// import App from "./App.js";
import handleClick from "./handleClick.js"

class  Lesson extends Component{
    state = {
        number: 10,
        count: 5,
    };
    addNumber = () =>{
        this.setState((prev)=>({
            number: prev.number + (10 - prev.count)
        }));
    };
    removeNumber = () => {
        const {number} = this.state
        if (number !== 0) {
            this.setState((prev)=>({
            number: prev.number - (10 - prev.count)
        }));
    }
    };
    aNumber = () => {
        const {count} = this.state
        if (count !== 10) {
            this.setState((prev)=>({
                count: prev.count + 1
            }));
        }
    };
    rNumber = () => {
        const {count} = this.state
        if (count !== 0) {
            this.setState((prev)=>({
                count: prev.count - 1
            }));
        }
    };
    render(){
        const {number,count} = this.state
        return(
            <div>
            <div>
                <button onClick={this.removeNumber}>-</button>
                <span>{number}</span>
                <button onClick={this.addNumber}>+</button>
            </div>
            <div>
               <button onClick={this.rNumber}>-</button>
                <span>{count}</span>
                <button onClick={this.aNumber}>+</button>
            </div>
            </div>
        )
    }
}


export default Lesson

















// function Welcome() {
//     return <h1>Hello World!</h1>
// }


// class SuperWelcome extends Component{
//     render(){
//         ///синтаксис написания JSX
//         return <h1>Hello World!</h1>
//     }
// }

// function Lesson(){
//     return (
//         <div>
//             <Welcome />
//             <SuperWelcome />
//         </div>
//     )
// }

// export default Lesson