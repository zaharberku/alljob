

function App(props) {
    const {article} = props 
    const body = <section>{article.text}</section>
    return(
        <div>
            <h1>{article.title}</h1>
            {body}
        </div>
    )
    
}


export default App