import React from 'react';
import ReactDOM, {render} from 'react-dom';
import Lesson from './components/Lesson'

// const element = React.createElement('div', {className:'div div-2'},[<h1>Hello World</h1>,<h1>Hello World</h1>])
//  const input = React.createElement('input', {className:'just-class'})
// console.log(input);

render(<Lesson/>, document.getElementById('root'))

// function HelloWorld() {
//   return (
//     <div>
//      <h1>Hello World</h1>
//     </div>
//   )
// }

// render(<HelloWorld/>, document.getElementById('root'))


// ReactDOM.render(element, document.getElementById('root'))
