import React,{useState, useEffect} from 'react'
import {Loading,Film} from './components'
import './App.css'

const App = () =>{
  const url = 'https://ajax.test-danit.com/api/swapi/films'
  const [arrFilms, setFilms] = useState([])
  const [loading, setLoading] = useState(true)
  
  
  useEffect(async () => {
    const data = await fetch(url).then(resolve=>resolve.json())
    setFilms(data)
    setLoading(false)
  },[])

  return (
    <ol className="App">
      {loading && <Loading/>}
      { arrFilms.length !== 0 ? arrFilms.map(element=>(<Film key={element.id} film={element}/>)) : null}
    </ol>
  );

}



export default App;
