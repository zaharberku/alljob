import React,{useState} from 'react'
import ListCharacters from '../ListCharacters'
import Loading from '../Loading'





const Film = ({film:{name,episodeId,openingCrawl,characters}}) =>{
    const [openCard, setCard] = useState(false)
    const [character, setCharacter] = useState([])
    const [loading, setLoading] = useState(true)
     
    
   const requestOncharacters = async () => {
        const data = await Promise.all(characters.map( async (element) => {
           const {name} = await fetch(element).then(resolve=>resolve.json())
           return name
        }))
        setCharacter(data)
        setLoading(false)
    }

    const openCardMore = () => {
        setCard(true)
        requestOncharacters()
    }

    const renderCharacter  = () => {
      return  character.map((element,index)=><ListCharacters key={index} person={element}/>)
    }


    return(
        <li>
        <p>Название:{name}</p>
        {!openCard ? <button onClick={()=>openCardMore()}>More</button> : 
        <div>
        <p>Описание:{openingCrawl}</p>
        <p>Идекс:{episodeId}</p>
        <ul>
        {loading ? <Loading/> : renderCharacter()}
        </ul>
        </div>
        } 
      </li>
    )
}


export default Film