import React from "react";
import style from './HeaderNav.module.scss'
import List from "../List";



const HeaderNav = (props) =>{
    const {dataItem} = props
    return(
        <nav className={style.headerNav}>
            <ul className={style.headerItem}>
                {dataItem.map(({name, id, link}) => <List key={id} text={name} link={link} />)}
            </ul>
        </nav>
    )
}

export default HeaderNav