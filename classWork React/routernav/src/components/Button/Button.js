import React from "react";
import style from "./Button.module.scss"

const Button = (props) =>{
    const {children,onClick} = props
    return (
        <button onClick={onClick}>{children}</button>
    )
}

export default Button 