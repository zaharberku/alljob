import React from "react";
import Button from "../Button"
import style from "./GoBack.module.scss"


const GoBack = (props) =>{
    const {history} = props

    const goBack = () => {
        history.goBack()
    }

    return(
        <>
        {
        history.length > 1 && 
        <div>
            <Button onClick={goBack}>Go back</Button>
        </div>
        }
        </>
    )
}


export default GoBack