import React from "react";
import style from "./Header.module.scss"
import HeaderNav from "../HeaderNav";
import UserData from '../UserData'


const Header = (props) =>{
    const {dataItem, title} = props
    return(
        <header className={style.header}>
            <h1>{title}</h1>
            <HeaderNav dataItem={dataItem} />
            <UserData/>
        </header>
    )
}


export default Header