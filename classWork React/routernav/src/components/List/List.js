import React from "react";
import style from './List.module.scss';
import { NavLink } from 'react-router-dom';

const List = (props) =>{
    const {text, link} = props
    return(
        <li className={style.headerList}><NavLink to={link}>{text}</NavLink></li>
    )
}


export default List