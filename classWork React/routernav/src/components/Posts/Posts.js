import React from "react";
import style from './Posts.module.scss';


const Posts = (props) =>{

    const {dataPost:{body, title, userId, id}, history, flag} = props

    return(
        <div className={style.postContainer} onClick={ () => flag && history.push(`Blog/${id}`)}>
            <span>userId:{userId}</span>
            <h2>{title}</h2>
            <p>{body}</p>
        </div>
    )
}


export default Posts