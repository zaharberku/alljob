import React from 'react'
import style from './App.module.scss'
import { Header } from './components'
import Router from './Router'



const dataHeaderNav = {
  dataItms : [{name:'Home',id:1, link:'/'}, {name:'Blog',id:2, link:'/Blog'}, {name:'Users',id:3, link:'/Users'}],
  title: 'Titlte',
}



const App = () =>{
  return(
    <>
    <Header dataItem={dataHeaderNav.dataItms} title={dataHeaderNav.title}/>
    <Router/>
    </>
  )
}
export default App;
