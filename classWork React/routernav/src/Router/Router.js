import React from "react";
import style from "./Router.module.scss"
import {Switch,Route} from "react-router-dom"
import { Blog, Home, User, Post } from "../page";



const Router = () =>{
    return(
        <Switch>
            <Route exact path="/" component={Home}/>
            <Route exact path="/Blog" component={Blog}/>
            <Route exact path="/Blog/:id" component={Post}/>
            <Route exact path="/Users" component={User} />
        </Switch>
    )
}

export default Router