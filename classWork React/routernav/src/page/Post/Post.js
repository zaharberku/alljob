import React, {useEffect, useState} from "react";
import style from './Post.module.scss';
import { Posts, GoBack } from "../../components";

const Post = (props) =>{

    const [data, getPost] = useState({})
    const {match:{params}, history} = props

    useEffect(()=>{

        (async()=>{
            try{
              const result = await fetch(`https://ajax.test-danit.com/api/json/posts/${params.id}`).then(res => res.json())
              getPost(result)
            }catch(e){
      
            }
          })()
    },[])

    return(
        <section className={style.post}>
            <GoBack history={history}/>
            <Posts dataPost={data} flag={false}/>
        </section>
    )
}


export default Post