import React from "react";
import style from "./Home.module.scss"
import { GoBack } from "../../components";



const Home = (props) =>{
    const {history} = props
    return(
        <section>
            <GoBack history={history}/>
            <h1>Home</h1>
        </section>
    )
}


export default Home