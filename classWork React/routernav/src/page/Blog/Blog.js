import React, {useEffect, useState} from "react";
import style from "./Blog.module.scss"
import { Posts, GoBack } from "../../components";


const Blog = (props) =>{
    const {history} = props
    const [data, getData] = useState([])

    useEffect(()=>{
        (async()=>{
          try{
            const result = await fetch('https://ajax.test-danit.com/api/json/posts').then(res => res.json())
            getData(result)
          }catch(e){
          }
        })()
      },[])

    return(
        <section className={style.blog}>
          <GoBack history={history}/>
            <div className={style.postsContainer}>
                {data.map(element=><Posts key={element.id} dataPost={element} history={history} flag={true}/>)}
            </div>
        </section>
    )
}


export default Blog