import React from "react";
import { Routes, Route,Navigate } from "react-router-dom";
import { Header, Main, Footer, LayOut, Id,LoginPage,RequireAuth } from './components'
import "./App.scss"


const App = () => (
  <Routes>
    <Route path="/" element={<LayOut />}>
      <Route index element={<Header />}/>
      <Route path="/Main" element={<Main />}/>
      <Route path="/Main/:id" element={<Id />}/>
      <Route path="/Main/new" element={
        <RequireAuth>
          <Navigate to="/Main/1"/>
        </RequireAuth>
      }/>
      <Route path="login" element={<LoginPage/>}/>
      <Route path="/Footer" element={<Footer />}/>
    </Route>
  </Routes>
);

export default App