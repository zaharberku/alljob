import Header from "./Header"
import Main from "./Main"
import Footer from "./Footer"
import LayOut from "./LayOut"
import CustomLink from "./CustomLink"
import Id from "./Id"
import LoginPage from "./LoginPage"
import RequireAuth from "./hoc/RequireAuth"



export {RequireAuth, Header, Main, Footer, LayOut, CustomLink, Id, LoginPage} 