import { Outlet } from "react-router-dom";
import CustomLink  from "../CustomLink";

// const setActive = ({isActive}) => isActive ? 'active-line' : ''

const LayOut = () => (
    <>
    <header>
        <nav>
            <ul>
                <li><CustomLink to="/">Header</CustomLink></li>
                <li><CustomLink to="/Main">Main</CustomLink></li>
                <li><CustomLink to="/Footer">Footer</CustomLink></li>
            </ul>
        </nav>
    </header>
    <Outlet/>
    </>
)


export default LayOut