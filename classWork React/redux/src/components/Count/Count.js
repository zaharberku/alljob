import React, {useState} from "react";
import { useSelector, useDispatch } from "react-redux";

const Count = () =>{
    const count = useSelector(state => state.count)

    const [inputVal, setInputValue] = useState('')

    const dispatch = useDispatch()

    const decrease = () => {
        dispatch({type:'DECREASE'})
    }

    const increase = () => {
        dispatch({type:'INCREASE'})

    }

    const setValueCount = () => {
        dispatch({type:'SET_VALUE_COUNT', payload:inputVal})
    }

    return(
        <div>
            <h1>{count}</h1>
            <button onClick={increase}>+</button>
            <button onClick={decrease}>-</button>
            <input type={"text"} onChange={({target})=> setInputValue(target.value)}/>
            <button onClick={setValueCount}>setValueCount</button>
        </div>
    )
}

export default Count