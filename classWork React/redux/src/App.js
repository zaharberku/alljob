import './App.module.scss';
import { createStore } from "redux"
import { Count } from "./components"
import { Provider } from 'react-redux'
import { conposeWithDevTools } from 'redux-devtools-extension'
const App = () => {

  const a = {
    count:0
  }

  const reduce = (state = a, action) =>{
      switch(action.type){
        case 'DECREASE':
          return {...state, count: state.count - 1}
        case 'INCREASE':
          return {...state, count: state.count + 1}
        case 'SET_VALUE_COUNT':
          return {...state, count: action.payload}
        default: 
          return state
      }
  }


  const store = createStore(reduce)
  return (
    <Provider store={store}>
    <div className="App">
     <Count/>
    </div>
    </Provider>
  );
}

export default App;
