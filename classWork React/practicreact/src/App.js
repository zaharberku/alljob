import style from './App.module.scss';
import React, {Component} from 'react';
import {Header, Main} from './components'


// https://ajax.test-danit.com/api/json/posts


class App extends Component {
  state = {
    title: 'Hello World',
    user:{
      name: 'Nazar',
    },
    emails:[
      {
        id:1,
        from:"The Postman Team",
        topic: "Announoing Postman's New Plans and Pricing",
        body: 'We are continuously adding capabilities to our platform-based on what you'
      },
      {
        id:2,
        from:"The Postman",
        topic: "Announoing Postman's New Plans and Pricing",
        body: 'We are continuously adding capabilities to our platform-based on what you'
      },
      {
        id:3,
        from:"The Team",
        topic: "Announoing Postman's New Plans and Pricing",
        body: 'We are continuously adding capabilities to our platform-based on what you'
      }
    ]
  }
  // changeTitle(){
  //   this.setState({
  //     title: this.state.inputValue,
  //     inputValue:'',
  //   })
  // }

  // upAge= () =>{
  //   this.setState(({user})=>({
  //     user:{
  //       ...user,
  //       age: user.age + 1

  //     }
  //   }))
  // }

  // handleChage=({target:{value}})=>{
  //   this.setState({inputValue:value})
  // }


  readMessages = (id) =>{
    this.setState((current)=>{
      const index = current.emails.findIndex(({id:elemId})=>{
        return id === elemId
      })

      const newState = {...current}
      newState.emails[index].isRead = true
      return newState
    })
  }

  render(){
    const {user:{name},title,emails} = this.state
    return (
      <div className={style.App}>
      <Header title={title} name={name} countMessenger={emails.length} />  
      
      <Main emails={emails} readMessages={this.readMessages}/>
      </div>

      // <div className={style.App}>
      //   {/* <Header incrAge={this.upAge}
      //   actions = {<button onClick={this.upAge}>incrAge</button>}
      //   >
      //     <span>Hello</span>
      //     <span>World</span>
      //   </Header> */}
      //  <h2>{title}</h2>
      //  <div>
      //    <p>Имя:{name}</p>
      //    <p>Возраст:{age}</p>
      //    <p>Хобби:{hobby ? hobby : 'Хобби нет'}</p>
      //  </div>
      //  <input type="text" onChange={this.handleChage} value={inputValue} />
      //  <button onClick={this.upAge}>Increment Age</button>
      //  <button onClick={this.changeTitle.bind(this)}>Update title</button>
      // </div>
    );
  }
}

export default App;
