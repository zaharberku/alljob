import React, {Component} from 'react'
import style from './index.module.scss'
import PropTypes from 'prop-types';


class Main extends Component{
    render(){
        const {emails, readMessages} = this.props
        return(
            <>
            {emails.map(element=>{
            const { id, from, topic, body} = element
            return(<div key={id} className={style.container} onClick={()=>readMessages(id)}>
              <span className={style.from}>{from}</span>
              <h3 className={style.topic}>{topic}</h3>
              <p className={style.body}>{body}</p>
              </div>)
          })}
          </>
          )
    }
}


// Main.propTypes = {
//     emails: PropTypes.oneOfType([
//         PropTypes.string,
//         PropTypes.array,
//     ]),
//     readMessages: PropTypes.func,
// }
// Main.defaultProps = {
//     emails: [],
//     readMessages: ()=>{},
// }

export default Main