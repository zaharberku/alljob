import React, {Component} from 'react';
import style from './index.module.scss';
import {avatar} from './img';
import PropTypes from 'prop-types';

class Header extends Component{
    render(){
        const {title, name, countMessenger} = this.props
        return(
            <header className={style.header}>
                <div className={style.headerLeft}>
                    <h1 className={style.headerTitle}>{title}</h1>
                    <span className={style.headerMain}>Email:{countMessenger}</span>
                </div>
                <div className={style.headerRight}>
                 <img className={style.headerImg} src={avatar} alt="avatar"/>
                 <span>{name}</span>
                </div>
            </header>
        )
    }
}

Header.propTypes = {
    title: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    countMessenger: PropTypes.number.isRequired,
}


// Header.defaultProps = {
//     title: 'Hellll',
//     name: 'Ivan',
//     countMessenger: 0,
// }

export default Header