// import './App.scss';
import React, {Component} from 'react';


class App extends Component{
  state = {
    number: 10,
    count: 5,
};
addNumber = () =>{
    this.setState((prev)=>({
        number: prev.number + prev.count
    }));
};
removeNumber = () => {
    const {number} = this.state
    if (number !== 0) {
        this.setState((prev)=>({
        number: prev.number -  prev.count
    }));
}
};
aNumber = () => {
    const {count} = this.state
    if (count !== 10) {
        this.setState((prev)=>({
            count: ++prev.count
        }));
    }
};
rNumber = () => {
    const {count} = this.state
    if (count !== 1) {
        this.setState((prev)=>({
            count: --prev.count
        }));
    }
};
render(){
    const {number,count} = this.state
    return(
        <div>
        <div>
            <button onClick={this.removeNumber}>-</button>
            <span>{number}</span>
            <button onClick={this.addNumber}>+</button>
        </div>
        <div>
           <button onClick={this.rNumber}>-</button>
            <span>{count}</span>
            <button onClick={this.aNumber}>+</button>
        </div>
        </div>
    )
}
}

export default App