import React, {Component} from 'react'
import './App.scss';
import {Header,Inbox} from './components';

class App extends Component {
  state = {
    user:{
        name: 'Name',
        id:1,
    },
    emails: [
      {
          id:0,
          topic:'Topic:0'
      },
      {
          id:1,
          topic:'Topic:1'
      },
      {
          id:2,
          topic:'Topic:2'
      },
  
  ]
   };

   logOut = () =>{
     this.setState({
      user:{
        name:null
      }
     })
   }


  deleteEmail = (id) => {
    this.setState({
      emails:this.state.emails.filter(element => element.id !== id)
    })
   
}
  render(){
    const {user,emails} = this.state
    return (
      <div className="App">
        <Header user={user} logOut={this.logOut} />
        <Inbox emails={emails} deleteEmail={this.deleteEmail}/>
      </div>
    );
  } 
}

export default App;
