import React,{Component} from "react";
import Email from '../Email'

class Inbox extends Component{
    render(){
        const {emails,deleteEmail} = this.props
        return(
            <div className="inbox">
                {emails.map(({id,topic})=><Email key={id} topic={topic} id={id} deleteEmail={deleteEmail}/>)}
            </div>
        )
    }
}


export default Inbox