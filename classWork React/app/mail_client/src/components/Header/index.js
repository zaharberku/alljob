import React,{Component} from "react";

class Header extends Component{
    render(){
        const {user:{name},logOut} = this.props
        return(
            <div>
                <p>{name ? name : ''}</p>
                <button onClick={logOut}>{name ? 'Log out' : 'Log in'}</button>
            </div>
        )
    };
}

export default Header