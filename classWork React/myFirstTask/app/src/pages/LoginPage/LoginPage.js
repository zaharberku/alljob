import React from 'react'
import style from './LoginPage.module.scss';
import {MainLogin} from '../../components'


const LoginPage = ({data, logIn}) =>{
    return(
        <MainLogin data={data} logIn={logIn}/>
    )
}

export default LoginPage