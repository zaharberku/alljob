import React from 'react'
import { HeaderAuthorized } from '../../components'

const AuthorizedPage = ({data, logOut}) =>{
    return(
        <HeaderAuthorized data={data} logOut={logOut}/>
    )
}


export default AuthorizedPage