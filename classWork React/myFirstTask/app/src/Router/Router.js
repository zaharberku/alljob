import React, {useState} from "react";
import { Routes, Route, useNavigate, Navigate } from "react-router-dom";
import { AuthorizedPage, LoginPage } from "../pages";
import {RequireAuth} from "../components"

const Router = (props) =>{
    const [isLogin, setLogin] = useState(false)
    const {dataForm,dataHeaderAuthorized} = props
    const navigate = useNavigate()

    const logIn = () =>{
        setLogin(!isLogin)
        navigate("/Authorized")
    }

    const logOut = () =>{
        setLogin(!isLogin)
        navigate("/login")
    }

    return(
        <Routes>
            <Route path="/login" element={<LoginPage data={dataForm} logIn={logIn}/>}/>
            <Route path="/Authorized" element={<RequireAuth islogin={isLogin}> <AuthorizedPage logOut={logOut} data={dataHeaderAuthorized}/> </RequireAuth> }/>
            <Route path="/" element={<Navigate to="/login"/>}/>
            {/* <Route path="*" element={<NotFound/>}/> */}
        </Routes>
    )
}

export default Router