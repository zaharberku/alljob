import React from "react";
import style from "./Button.module.scss"

const Button = (props) =>{
    const {text, type, styleClass} = props
    return(
        <button className={styleClass} type={type}>{text}</button>
    )
}


export default Button