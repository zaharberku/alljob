import React from "react";
import style from "./User.module.scss"

const User = (props) => {
    const {changeState} = props
    return(
        <div className={style.user} onClick={changeState}>
            <span className={style.userName}>US</span>
        </div>
    )
}

export default User