import React from "react";
import style from "./MainLogin.module.scss"
import Img from "../Img";
import { heroImg, logo} from "../assets";
import Form from "../Form";

const Main = ({data, logIn}) =>{
    return(
        <main>
            <section className={style.sectionMain}>
                <Img styleClass={style.sectionMainImg} src={heroImg} alt={"heroHouse"} />
                <div className={style.mainBlockForm}>
                <Img styleClass={style.sectionMainLogo} src={logo} alt={"logo"} />
                <Form data={data} logIn={logIn}/>
                </div>
            </section>
        </main>
    )
}

export default Main