import React from "react";
import style from "./Input.module.scss"

const Input = (props) =>{
    const {type, placeholder, name, text} = props
    return(
        <label className={style.mainFormLabel}>{text}
        <input className={style.mainFormInput} name={name} type={type} placeholder={placeholder}/>
        </label>
    )
}

export default Input