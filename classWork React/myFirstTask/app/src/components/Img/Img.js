import React from "react";


const Img = (props) =>{
    const {src, alt, styleClass} = props
    return(
        <img className={styleClass} src={src} alt={alt}/>
    )
}


export default Img