import React, {useState} from "react";
import style from './HeaderAuthorized.module.scss'
import Button from "../Button";
import User from "../User";
import HeaderNav from "../HeaderNav";

const HeaderAuthorized = ({data, logOut}) => {
    const {title, navName} = data
    const [flag, setFLag] = useState(false)

    const changeState = () =>{
        setFLag(!flag)
    }

    return(
        <header className={style.header}>
            <div className={style.headeNavMenu}>
                <div className={style.navMenu}>
                    <Button styleClass={style.burgerMenu} text={<span></span>}/>
                    <h2 className={style.titleTextHeader}>{title}</h2>
                </div>
                <User changeState={changeState}/>
            </div>
           {flag && <HeaderNav navName={navName} logOut={logOut}/>}
        </header>
    )
}

export default HeaderAuthorized