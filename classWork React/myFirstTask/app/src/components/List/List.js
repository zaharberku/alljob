import React from "react";
import style from "./List.module.scss"

const List = (props) =>{
    const {name, onClick} = props
    return(
        <li onClick={onClick} className={style.list}>{name}</li>
    )
}

export default List