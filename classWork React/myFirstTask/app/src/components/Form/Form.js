import React, {useEffect, useState} from "react";
import { useNavigate } from "react-router-dom";
import style from "./Form.module.scss";
import Input from '../Input';
import Button from '../Button';

const Form = (props) =>{
    const {data:{inputs, title, button}, logIn} = props
    const [dataUser, setData] = useState({})
    const [dataPassword, setServ] = useState([])
    

    const validPassword = () =>{
        dataPassword.some(element=>{
           const stringPassword = element.email + " " + element.password
           const stringUser = Object.values(dataUser).join(' ')
           if (stringPassword === stringUser) {
            logIn()
           }
        })
    }

    const getDataForm = (event) =>{
        const {target} = event
        event.preventDefault()
        const value = [...target.elements].reduce((item,element)=>{
            const {name, value} = element
            if(name){
                item[name] = value
            }
            return item
        },{})
        setData(value)
    }


    useEffect(()=>{
        (async()=>{
            try{
                const resolve = await fetch('./package.json').then(res=>res.json())
                setServ(resolve)
                validPassword()
            }catch(e){
                console.log(e);
            }
            
        })()
    },[dataUser])

    return(
        <form className={style.mainForm} onSubmit={getDataForm}>
            <h2 className={style.mainFormTitle}>{title}</h2>
            {inputs.map(element=><Input key={element.id} {...element} />)}
            {button.map(element=> <Button styleClass={style.mainFormButton} key={element.id} {...element} />)}
        </form>
    )
}

export default Form