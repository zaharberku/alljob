import { Navigate } from "react-router-dom";



const RequireAuth = ({islogin,children}) =>{
    if (!islogin) {
        return <Navigate to="/login"/>
    }
    return children
}

export default RequireAuth