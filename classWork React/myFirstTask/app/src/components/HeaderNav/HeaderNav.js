import React from "react";
import style from "./HeaderNav.module.scss"
import List from "../List";

const HeaderNav = (props) => {
    const {navName, logOut} = props
    
    return(
        <ul className={style.headerNav}>
            {navName.map(({name, id})=><List key={id} name={name} onClick={name === 'Logout' ? logOut : ()=>{}}/>)}
        </ul>
    )
}

export default HeaderNav