import style from './App.module.scss';
import React from 'react';
import { dataHeaderAuthorized, dataForm } from './components/data';
import Router from './Router';
const App = () => {
  return (
    <>
    <div className={style.App}>
    <Router dataForm={dataForm} dataHeaderAuthorized={dataHeaderAuthorized}/>
    </div>
    
    </>
  );
}

export default App;
